<?php declare(strict_types = 1);

$ignoreErrors = [];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property object\\:\\:\\$is_tax\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^Action callback returns string but should not return anything\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function in_array\\(\\) requires parameter \\#3 to be set\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Admin\\:\\:column_display\\(\\) should return string but return statement is missing\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Admin\\:\\:product_filters_query\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the left side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @var has invalid value \\(\\$simple_supported_types\\)\\: Unexpected token "\\$simple_supported_types", expected type at offset 80$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$text of function esc_html expects string, int given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#4 \\$ver of function wp_enqueue_script expects bool\\|string\\|null, int\\<1, max\\> given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Admin\\:\\:\\$plugin_path is never read, only written\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Admin\\:\\:\\$simple_supported_types has no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Admin.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Install\\:\\:add_settings\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Install.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a negated boolean, mixed given\\.$#',
	'count' => 6,
	'path' => __DIR__ . '/src/Admin/Install.php',
];
$ignoreErrors[] = [
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\ProductFields\\:\\:add_enable_custom_price_input\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\ProductFields\\:\\:add_enable_custom_price_input\\(\\) has parameter \\$product_object with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\ProductFields\\:\\:add_to_variations_metabox\\(\\) has parameter \\$variation_data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\ProductFields\\:\\:get_product_fields\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\ProductFields\\:\\:get_product_fields\\(\\) has parameter \\$loop with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\ProductFields\\:\\:get_product_fields\\(\\) has parameter \\$product_object with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\ProductFields\\:\\:get_product_fields\\(\\) has parameter \\$show_billing_period_options with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\ProductFields\\:\\:product_variations_options\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\ProductFields\\:\\:product_variations_options\\(\\) has parameter \\$variation_data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#2 \\$single of method WC_Data\\:\\:get_meta\\(\\) expects bool, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/ProductFields.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function in_array\\(\\) requires parameter \\#3 to be set\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/SaveProductMeta.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\SaveProductMeta\\:\\:save_product_variation\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/SaveProductMeta.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Admin\\\\Product\\\\SaveProductMeta\\:\\:save_product_variation\\(\\) has parameter \\$variation with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/SaveProductMeta.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the left side\\.$#',
	'count' => 4,
	'path' => __DIR__ . '/src/Admin/Product/SaveProductMeta.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the right side\\.$#',
	'count' => 4,
	'path' => __DIR__ . '/src/Admin/Product/SaveProductMeta.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed int\\|WC_Product_Variation \\$variation\\)\\: Unexpected token "int", expected variable at offset 72$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/SaveProductMeta.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$variation_min_price might not be defined\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/SaveProductMeta.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable \\$variation_suggested_price might not be defined\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Admin/Product/SaveProductMeta.php',
];
$ignoreErrors[] = [
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Admin/views/html-admin-page-status-report.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_scalar\\(\\) with int will always evaluate to true\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to static method get_length\\(\\) on an unknown class WC_Subscriptions_Product\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to static method get_period\\(\\) on an unknown class WC_Subscriptions_Product\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Casting to int something that\'s already int\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Instanceof between WC_Product and WC_Product will always evaluate to true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:add_cart_item_data\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:add_cart_item_data\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:check_cart_items\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:edit_in_cart_redirect_message\\(\\) has parameter \\$message with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:get_cart_item_from_session\\(\\) has parameter \\$cart_item with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:get_cart_item_from_session\\(\\) has parameter \\$values with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:get_cart_item_from_session\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:set_cart_item\\(\\) has parameter \\$cart_item with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:set_cart_item\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:validate_add_cart_item\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Cart\\:\\:validate_add_cart_item\\(\\) has parameter \\$passed with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, mixed given on the left side\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the left side\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the right side\\.$#',
	'count' => 6,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a ternary operator condition, int given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a ternary operator condition, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in an if condition, string given\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$price of function wc_price expects float, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$val of function is_infinite expects float, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Result of \\|\\| is always false\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^The overwriting return is on this line\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^This return is overwritten by a different one in the finally block below\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^This throw is overwritten by a different one in the finally block below\\.$#',
	'count' => 4,
	'path' => __DIR__ . '/src/Cart.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product\\:\\:\\$get_id\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to an undefined property WC_Product\\:\\:\\$variation_id\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to protected property WC_Product\\:\\:\\$id\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_object\\(\\) with WC_Product will always evaluate to true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Cannot call method get_price_html_from_text\\(\\) on int\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Else branch is unreachable because previous condition is always true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Core\\\\CoreCompatibility\\:\\:get_prop\\(\\) has parameter \\$name with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Core\\\\CoreCompatibility\\:\\:set_prop\\(\\) has parameter \\$name with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Core\\\\CoreCompatibility\\:\\:set_prop\\(\\) has parameter \\$obj with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the left side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the right side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param references unknown parameter\\: \\$product$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param references unknown parameter\\: \\$prop$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Core\\\\CoreCompatibility\\:\\:\\$is_wc_version_gt type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Core\\\\CoreCompatibility\\:\\:\\$is_wc_version_gte type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Right side of && is always true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable method call on mixed\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable method call on object\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable property access on mixed\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Variable property access on object\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Core/CoreCompatibility.php',
];
$ignoreErrors[] = [
	'message' => '#^Implicit array creation is not allowed \\- variable \\$extension_classes does not exist\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/ExtensionSupport.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\ExtensionSupport\\:\\:load_extensions\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/ExtensionSupport.php',
];
$ignoreErrors[] = [
	'message' => '#^Default value of the parameter \\#4 \\$variation_id \\(string\\) of method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\CoCart\\:\\:add_to_cart_validation\\(\\) is incompatible with type int\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/CoCart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\CoCart\\:\\:add_to_cart_validation\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/CoCart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\CoCart\\:\\:add_to_cart_validation\\(\\) has parameter \\$passed with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/CoCart.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\CoCart\\:\\:add_to_cart_validation\\(\\) has parameter \\$variations with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/CoCart.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a ternary operator condition, int given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/CoCart.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param references unknown parameter\\: \\$variation$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/CoCart.php',
];
$ignoreErrors[] = [
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/GroupedProducts.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\GroupedProducts\\:\\:add_filter_for_cpw_attributes\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/GroupedProducts.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\GroupedProducts\\:\\:optional_cpw_attributes\\(\\) has parameter \\$attributes with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/GroupedProducts.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\GroupedProducts\\:\\:optional_cpw_attributes\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/GroupedProducts.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\GroupedProducts\\:\\:remove_filter_for_cpw_attributes\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/GroupedProducts.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#2 \\$args of method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_price_input\\(\\) expects array, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/GroupedProducts.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\QV\\:\\:load_scripts\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/QV.php',
];
$ignoreErrors[] = [
	'message' => '#^Callback expects 2 parameters, \\$accepted_args is set to 1\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/Stripe.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in \\|\\|, string given on the right side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/Stripe.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to an undefined method WC_Product\\:\\:get_variation_prices\\(\\)\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/VariableProducts.php',
];
$ignoreErrors[] = [
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/VariableProducts.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a ternary operator condition, WC_Product given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/VariableProducts.php',
];
$ignoreErrors[] = [
	'message' => '#^Ternary operator condition is always true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/VariableProducts.php',
];
$ignoreErrors[] = [
	'message' => '#^Casting to bool something that\'s already bool\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCPay.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in \\|\\|, string given on the right side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCPay.php',
];
$ignoreErrors[] = [
	'message' => '#^Access to static property \\$option_prefix on an unknown class WC_Subscriptions_Admin\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Filter callback return statement is missing\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Function wcs_get_order_item not found\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Function wcs_get_subscription not found\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:add_switch_query_args\\(\\) has parameter \\$item_id with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:allow_switching_options\\(\\) has parameter \\$data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:allow_switching_options\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:customize_single_variable_product\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:disable_input\\(\\) has parameter \\$attributes with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:disable_input\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:hide_edit_link_in_cart\\(\\) has parameter \\$cart_item with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:is_identical_product\\(\\) has parameter \\$item with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:is_identical_product\\(\\) has parameter \\$product_id with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:is_identical_product\\(\\) has parameter \\$quantity with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:is_identical_product\\(\\) has parameter \\$subscription with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:is_identical_product\\(\\) has parameter \\$variation_id with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:is_switchable\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:is_switchable\\(\\) has parameter \\$variation with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, int given on the left side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the left side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a ternary operator condition, int given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in an if condition, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(WC_Product\\)\\: Unexpected token "\\\\n\\\\t \\* ", expected variable at offset 117$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed null\\|WC_Product_Variation\\)\\: Unexpected token "null", expected variable at offset 135$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param references unknown parameter\\: \\$is_product_switchable$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param references unknown parameter\\: \\$product$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\$subscription of method WPDesk\\\\Library\\\\CustomPrice\\\\Compatibility\\\\Extensions\\\\WCSubscriptions\\:\\:switch_validation\\(\\) has invalid type WC_Subscription\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Compatibility/Extensions/WCSubscriptions.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to function is_string\\(\\) with array will always evaluate to false\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to static method get_sign_up_fee\\(\\) on an unknown class WC_Subscriptions_Product\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to static method get_trial_length\\(\\) on an unknown class WC_Subscriptions_Product\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Casting to array something that\'s already array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Default value of the parameter \\#2 \\$suffix \\(false\\) of method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_variable_price_input\\(\\) is incompatible with type string\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Default value of the parameter \\#3 \\$post_id \\(string\\) of method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:add_post_class\\(\\) is incompatible with type int\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Foreach overwrites \\$period with its value variable\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^If condition is always false\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:add_edit_link_in_cart\\(\\) has parameter \\$cart_item with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:add_post_class\\(\\) has parameter \\$classes with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:add_post_class\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:add_to_cart_text\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:add_to_cart_url\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:available_variation\\(\\) has parameter \\$data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:available_variation\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_error_holder\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_minimum_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_price_input\\(\\) has parameter \\$args with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_price_input\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_suggested_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_variable_billing_periods\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_variable_billing_periods\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_variable_price_input\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_variable_price_input\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:edit_quantity\\(\\) has parameter \\$args with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:edit_quantity\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:get_variation_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:register_scripts\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:replace_price_template\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:restore_price_template\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:supports_ajax_add_to_cart\\(\\) has parameter \\$feature with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:supports_ajax_add_to_cart\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:supports_ajax_add_to_cart\\(\\) has parameter \\$supports_ajax with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:variation_is_visible\\(\\) has parameter \\$variation with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Negated boolean expression is always false\\.$#',
	'count' => 4,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^One or more @param tags has an invalid name or invalid syntax\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the left side\\.$#',
	'count' => 4,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the right side\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a negated boolean, WC_Product given\\.$#',
	'count' => 4,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a negated boolean, int given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a negated boolean, string given\\.$#',
	'count' => 5,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a ternary operator condition, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in an elseif condition, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in an if condition, string given\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in \\|\\|, string given on the right side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(WC_Product_Variation\\)\\: Unexpected token "\\\\n\\\\t \\*", expected variable at offset 198$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 58$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 60$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 82$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 86$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 94$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param references unknown parameter\\: \\$url$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$text of function esc_attr expects string, int given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#2 \\$args of method WPDesk\\\\Library\\\\CustomPrice\\\\Display\\:\\:display_price_input\\(\\) expects array, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#3 \\$deps of function wp_enqueue_style expects array\\<string\\>, false given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#3 \\$template_path of function wc_get_template expects string, false given\\.$#',
	'count' => 3,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#4 \\$ver of function wp_enqueue_style expects bool\\|string\\|null, int\\<1, max\\> given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#4 \\$ver of function wp_register_script expects bool\\|string\\|null, int\\<1, max\\> given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Display.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to an undefined method WC_Product\\:\\:get_variation_price\\(\\)\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to static method get_interval\\(\\) on an unknown class WC_Subscriptions_Product\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to static method get_length\\(\\) on an unknown class WC_Subscriptions_Product\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to static method get_period\\(\\) on an unknown class WC_Subscriptions_Product\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to static method get_price_string\\(\\) on an unknown class WC_Subscriptions_Product\\.$#',
	'count' => 4,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Call to static method get_subscription_period_strings\\(\\) on an unknown class WPDesk\\\\Library\\\\CustomPrice\\\\WC_Subscriptions_Manager\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Construct empty\\(\\) is not allowed\\. Use more strict comparison\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Default value of the parameter \\#1 \\$product \\(false\\) of method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_posted_period\\(\\) is incompatible with type string\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Default value of the parameter \\#2 \\$suffix \\(false\\) of method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_posted_period\\(\\) is incompatible with type string\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Default value of the parameter \\#2 \\$suffix \\(false\\) of method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_posted_price\\(\\) is incompatible with type string\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Default value of the parameter \\#2 \\$suffix \\(false\\) of method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_price_input\\(\\) is incompatible with type string\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Foreach overwrites \\$period with its value variable\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Function wcs_get_subscription_period_strings not found\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Left side of && is always true\\.$#',
	'count' => 4,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:annual_price_factors\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:annualize_price\\(\\) has parameter \\$period with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:annualize_price\\(\\) has parameter \\$price with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:error_message\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:error_message\\(\\) has parameter \\$tags with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:format_price\\(\\) has parameter \\$args with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_data_attributes\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_initial_period\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_initial_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_maximum_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_maximum_price\\(\\) should return string but returns false\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_maximum_price_html\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_maximum_price_html\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_minimum_billing_period\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_minimum_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_minimum_price_html\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_minimum_price_html\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_minimum_variation_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_minimum_variation_price\\(\\) should return string but returns false\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_period_value_attr\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_period_value_attr\\(\\) has parameter \\$suffix with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_posted_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_price_input\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_price_input_label_text\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_price_string\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_price_terms_html\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_price_value_attr\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_price_value_attr\\(\\) has parameter \\$suffix with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_simple_supported_types\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_subscription_period_input\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_subscription_period_strings\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_subscription_period_strings\\(\\) has parameter \\$number with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_subscription_period_strings\\(\\) has parameter \\$period with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_subscription_terms\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_subscription_terms_html\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_suggested_billing_period\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_suggested_price\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_suggested_price_html\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:get_variable_supported_types\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:has_cpw\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:has_cpw\\(\\) should return string but returns false\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:increase_counter\\(\\) should return int but return statement is missing\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:is_billing_period_variable\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:is_billing_period_variable\\(\\) should return string but returns false\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:is_cpw\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:is_maximum_hidden\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:is_minimum_hidden\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:is_subscription\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:is_variable_price_hidden\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:maybe_get_product_instance\\(\\) has parameter \\$product with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:standardize_number\\(\\) has parameter \\$value with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Negated boolean expression is always false\\.$#',
	'count' => 10,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^One or more @param tags has an invalid name or invalid syntax\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, WC_Product given on the left side\\.$#',
	'count' => 4,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, mixed given on the left side\\.$#',
	'count' => 3,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the right side\\.$#',
	'count' => 3,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a negated boolean, WC_Product given\\.$#',
	'count' => 10,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a negated boolean, mixed given\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a negated boolean, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in a ternary operator condition, string given\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in an elseif condition, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in an if condition, string given\\.$#',
	'count' => 3,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in \\|\\|, string given on the left side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in \\|\\|, string given on the right side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed int\\|obj \\$product\\)\\: Unexpected token "int", expected variable at offset 21$#',
	'count' => 3,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed int\\|obj \\$product\\)\\: Unexpected token "int", expected variable at offset 61$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed int\\|object \\$product\\)\\: Unexpected token "int", expected variable at offset 57$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 116$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 21$#',
	'count' => 11,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 46$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 47$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 52$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 54$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\)\\: Unexpected token "obj", expected variable at offset 75$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(mixed obj\\|int \\$product\\.\\)\\: Unexpected token "obj", expected variable at offset 21$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param references unknown parameter\\: \\$product_id$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param references unknown parameter\\: \\$suffix$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @return has invalid value \\(\\$price string\\)\\: Unexpected token "\\$price", expected type at offset 17$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$price of function wc_price expects float, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Parameter \\#1 \\$price of static method WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:format_price\\(\\) expects float, string given\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:\\$simple_supported_types type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\CustomPrice\\\\Helper\\:\\:\\$variable_supported_types type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Strict comparison using \\!\\=\\= between \'\' and float will always evaluate to true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Strict comparison using \\!\\=\\= between false and string will always evaluate to true\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Helper.php',
];
$ignoreErrors[] = [
	'message' => '#^Property WPDesk\\\\Library\\\\CustomPrice\\\\Integration\\:\\:\\$logger is unused\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Integration.php',
];
$ignoreErrors[] = [
	'message' => '#^Function wcs_get_canonical_product_id not found\\.$#',
	'count' => 2,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^Function wcs_get_subscriptions_for_order not found\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Order\\:\\:find_subscription\\(\\) has no return type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Order\\:\\:find_subscription\\(\\) has parameter \\$order with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Order\\:\\:find_subscription\\(\\) has parameter \\$order_item with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Order\\:\\:order_again_cart_item_data\\(\\) has parameter \\$cart_item_data with no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Order\\:\\:order_again_cart_item_data\\(\\) has parameter \\$line_item with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Order\\:\\:order_again_cart_item_data\\(\\) has parameter \\$order with no type specified\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^Method WPDesk\\\\Library\\\\CustomPrice\\\\Order\\:\\:order_again_cart_item_data\\(\\) return type has no value type specified in iterable type array\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^Only booleans are allowed in &&, string given on the right side\\.$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(WC_Order\\)\\: Unexpected token "\\\\n\\\\t \\*", expected variable at offset 185$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];
$ignoreErrors[] = [
	'message' => '#^PHPDoc tag @param has invalid value \\(WC_Order_Item_Product \\(supports array notation due to ArrayAccess\\)\\)\\: Unexpected token "\\(", expected variable at offset 121$#',
	'count' => 1,
	'path' => __DIR__ . '/src/Order.php',
];

return ['parameters' => ['ignoreErrors' => $ignoreErrors]];
