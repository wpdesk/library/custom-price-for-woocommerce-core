<?php
/**
 * Plugin Cross-Compatibility
 *
 * @package  WooCommerce Custom Price/Compatibility
 * @since    2.7.0
 * @version  3.0.0
 */

namespace WPDesk\Library\CustomPrice\Compatibility;

use WPDesk\Library\CustomPrice\Integration;
use WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\Library\CustomPrice\Cart;
use WPDesk\Library\CustomPrice\Compatibility\Core\CoreCompatibility;
use WPDesk\Library\CustomPrice\Compatibility\Extensions\CoCart;
use WPDesk\Library\CustomPrice\Compatibility\Extensions\GroupedProducts;
use WPDesk\Library\CustomPrice\Compatibility\Extensions\QV;
use WPDesk\Library\CustomPrice\Compatibility\Extensions\Stripe;
use WPDesk\Library\CustomPrice\Compatibility\Extensions\VariableProducts;
use WPDesk\Library\CustomPrice\Compatibility\Extensions\WCPay;
use WPDesk\Library\CustomPrice\Compatibility\Extensions\WCSubscriptions;
use WPDesk\Library\CustomPrice\Display;
use WC_Subscriptions;

/**
 * Handle loading of extensions depending on active plugins.
 */
class ExtensionSupport implements Hookable {
	/**
	 * @var Cart
	 */
	private $cart;
	/**
	 * @var Display
	 */
	private $display;

	/**
	 * @param Cart    $cart
	 * @param Display $display
	 */
	public function __construct( Cart $cart, Display $display ) {
		$this->cart    = $cart;
		$this->display = $display;
	}

	public function hooks() {
		add_action( 'plugins_loaded', [ $this, 'load_extensions' ], 100 );
	}

	/**
	 * @since 1.0.0
	 */
	public function load_extensions() {

		// Variable products.
		$extension_classes['variable_products'] = new VariableProducts();
		if ( Integration::is_super() ) {
			// CoCart support.
			if ( defined( 'COCART_VERSION' ) ) {
				$extension_classes['cocart'] = new CoCart( $this->cart );
			}

			// Grouped products.
			if ( CoreCompatibility::is_wc_version_gte( '3.3.0' ) ) {
				$extension_classes['grouped_products'] = new GroupedProducts( $this->display );
			}

			// Subscriptions switching.
			if ( class_exists( 'WC_Subscriptions' ) && version_compare( WC_Subscriptions::$version, '1.4.0', '>' ) ) {
				$extension_classes['subscriptions'] = new WCSubscriptions();
			}

			// Stripe fixes.
			if ( class_exists( 'WC_Stripe' ) ) {
				$extension_classes['stripe'] = new Stripe();
			}

			// QuickView support.
			if ( class_exists( 'WC_Quick_View' ) ) {
				$extension_classes['quickview'] = new QV( $this->display );
			}

			// WooCommerce Payments request buttons.
			if ( class_exists( 'WC_Payments' ) ) {
				$extension_classes['wcpay'] = new WCPay();
			}
		}

		/**
		 * 'wc_cpw_compatibility_modules' filter.
		 * Use this to filter the required compatibility modules.
		 *
		 * @param array $extension_classes
		 *
		 * @since 1.0.0
		 */
		$extension_classes = apply_filters( 'wc_cpw_compatibility_modules', $extension_classes );
		foreach ( $extension_classes as $name => $extensions ) {
			if ( $extensions instanceof Hookable ) {
				$extensions->hooks();
			}
		}

	}

}
