## [1.1.2] - 2024-11-20
### Fixed
- Translations loading to early in wp 6.7

## [1.1.1] - 2024-07-11
### Fixed
- All Products woocommerce block does not redirects custom price products on product page

## [1.1.0] - 2024-03-18
### Added
- Support page

## [1.0.3] - 2023-10-09
### Fixed
- links

## [1.0.2] - 2023-08-17
### Fixed
- remove strict types from hook function add_cart_item_data

## [1.0.1] - 2022-09-06
### Added
- psr container
- pro url


## [1.0.0] - 2022-03-17
### Added
- init
